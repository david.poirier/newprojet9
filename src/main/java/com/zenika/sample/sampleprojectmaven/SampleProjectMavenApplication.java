package com.zenika.sample.sampleprojectmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleProjectMavenApplication {

	public static void main(String[] args) {
		if (false) {
			System.out.println("FAUX");
		}
		String variable = "toto";
		if ("toto".equals("titi")) {
			System.out.println("FAUX");
		}
		SpringApplication.run(SampleProjectMavenApplication.class, args);
	}

}
